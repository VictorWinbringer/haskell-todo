{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Web.Scotty
import Database.PostgreSQL.Simple
import Data.Monoid (mconcat)
import Data.Int
import Database.PostgreSQL.Simple.FromRow
import Control.Monad.IO.Class
import Data.Aeson (ToJSON)
import GHC.Generics
import Data.String (fromString)

localPG :: ConnectInfo
localPG = defaultConnectInfo
        { connectHost = "127.0.0.1"
        , connectDatabase = "haskell_todo_test"
        , connectUser = "postgres"
        , connectPassword = "postgres"
        }

data Todo = Todo { id :: Int, name :: String }
  deriving (Show, Generic)

instance FromRow Todo where
    fromRow = Todo <$> field <*> field

instance ToJSON Todo

retrieveTodo :: Connection -> Int -> IO [Todo]
retrieveTodo conn cid = query conn "SELECT id, name FROM todos WHERE id = ?" $ (Only cid)

createTodo :: Connection -> String -> IO [Only Int64]
createTodo conn name =
  query conn "INSERT INTO todos (name) VALUES (?) RETURNING id" $ (Only name)

updateTodo :: Connection -> Int -> String -> IO Bool
updateTodo conn cid name = do
    n <- execute conn "UPDATE todos SET name = ? WHERE id = ?" (name, cid)
    return $ n > 0

removeTodo :: Connection -> Int -> IO Bool
removeTodo conn cid = do
  n <- execute conn "DELETE FROM todos WHERE id = ?" $ (Only cid)
  return $ n > 0

getTodo :: ScottyM()
getTodo =    get "/todos/:id" $ do
  id <- param "id"
  conn <- liftIO (connect localPG)
  todo <- liftIO (retrieveTodo conn id)
  json $ (todo::[Todo])

postTodo :: ScottyM()
postTodo =    post "/todos/:name" $ do
  name <- param "name"
  conn <- liftIO (connect localPG)
  [Only id] <- liftIO (createTodo conn name)
  text $ fromString $ (show id)

putTodo :: ScottyM()
putTodo =    put "/todos/:id/:name" $ do
  id <- param "id"
  name <- param "name"
  conn <- liftIO (connect localPG)
  success <- liftIO (updateTodo conn id name)
  text $ fromString $ (show success)

deleteTodo :: ScottyM()
deleteTodo =    delete "/todos/:id" $ do
  id <- param "id"
  conn <- liftIO (connect localPG)
  success <- liftIO (removeTodo conn id)
  text $ fromString $ (show success)

main = scotty 3000 $ getTodo >> postTodo >> putTodo >> deleteTodo

